//this code is an implementation of basic functions(get and set) of LRU Cache(least recently used cache),
//the implementation of LRU Cache including a double linked list(built by myself) and a hashmap(built in Java).
//the LRU Cache is in access order, which means once you get or set some element, it will be put at the head of the cache


package hw1;
import java.util.*;

public class LRU {
    HashMap<Integer, ListNode> map = new HashMap<Integer, ListNode>();
    private class ListNode {
        ListNode prev = null;
        ListNode next = null;
        int key;
        int val;
    }
    private ListNode head;
    private ListNode tail;
    private int count;
    private int capacity;
    private void moveToHead(ListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
        node.next = head.next;
        node.prev = head;
        node.next.prev = node;
        node.prev.next = node;
    }
    
    private void addNode(ListNode node) {
        node.next = head.next;
        node.prev = head;
        node.next.prev = node;
        node.prev.next = node;
    }
    
    private void poptail() {
        ListNode remove = tail.prev;
        tail.prev = remove.prev;
        tail.prev.next = tail;
        map.remove(remove.key);
    }
    
    public LRU(int capacity) {
    	try {
        head = new ListNode();
        tail = new ListNode();
        this.count = 0;
        this.capacity = capacity;
        head.next = tail;
        tail.prev = head;
    	} catch(Exception e) {
    		System.out.println("constructor error");
    	}
    }
    
    public int get(int key) {
    	try {
        ListNode curr = map.get(key);
        if (curr != null) {
        moveToHead(curr);
        return curr.val;
        }
        else {
            return -1;
        }
    	} catch (Exception e) {
    		System.out.println("get function error");
    		return -1;
    	}
    }
    
    public void set(int key, int value) {
    	try {
        if (!map.containsKey(key)) {
            ListNode newNode = new ListNode();
            newNode.key = key;
            newNode.val = value;
            addNode(newNode);
            map.put(key, newNode);
            count++;
            if (count > capacity) {
                poptail();
                count--;
            }
        }
        else {
            ListNode curr = map.get(key);
            curr.val = value;
            moveToHead(curr);
            map.put(key, curr);
        }
    	} catch (Exception e) {
    		System.out.println("set function error");
    	}
    }
    public static void main(String[] args) {
    	LRU cache = new LRU(5);
    	cache.set(1, 10);
    	cache.set(2, 20);
    	cache.set(3, 30);
    	cache.set(4, 40);
    	cache.set(5, 50);
    	System.out.println("before get 3");
    	ListNode curr = cache.head;
    	while (curr.next != null) {
    		curr = curr.next;
    		System.out.println(curr.key);
    	}
    	cache.get(3);
    	System.out.println("after get 3");
    	curr = cache.head;
    	while (curr.next != null) {
    		curr = curr.next;
    		System.out.println(curr.key);
    	}
    }
}
